/**
 * 
 */

/**
 * @author Raveendar Bandaru
 *
 */
public class TestLoop {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i = 10 ;
		
		//pre-increment and post-increment
		// ++x 
		//x++ (x = x+1)
		
		//for loop
		int x=0;
		
		for(; x < i;) {
			//statements
			System.out.println(x);
			x = x + 1 ;
		}
		
	   System.out.println("value of x = "+x);
	   x = 0;
		
	   while(x < i) {
			//statements
			System.out.println(x);
			x++;
		}
	   
	   //
	   x = 100;
	   System.out.println("post-increment "+ x++); //100
	   System.out.println(""+ x);
	   
	   x=100;
	   System.out.println("post-decrement "+ x--);
	   System.out.println(""+ x);
	  
	   x=100;
	   System.out.println("pre-increment "+ (++x));
	   System.out.println(""+ x);
	  
	   x=100;
	   System.out.println("pre-decrement "+ (--x));
	   System.out.println(""+ x);
	   
	   // do - while
	   x=100;
	   do{ 
		   //statements
		   System.out.println("I am in do block "+ x);
		   x--;
	   } while( x > 90);
	   
	   
	   // enhanced for loop
	   // Enumeration
	   // iterator 
	   // for each
	   // 
	   
	   // I have added this new comment 
	   
	}//end of main

}
